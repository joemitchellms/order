package shop.velox.order.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.PaymentStatus;
import shop.velox.order.api.dto.ShipmentStatus;
import shop.velox.user.api.dto.UserDto;


public class OrderEntity {

  @Id
  private String id;

  private String orderNum;

  private String userId;

  private LocalDateTime createTime;

  private List<OrderEntryEntity> entries;

  private AddressEntity shippingAddress;

  private AddressEntity billingAddress;

  private List<ChargeEntity> charges;

  private BigDecimal totalPrice;

  private String currencyId;

  private String cartId;

  private OrderStatus orderStatus;

  private PaymentStatus paymentStatus;

  private ShipmentStatus shipmentStatus;

  private UserDto user;

  public OrderEntity() {
    //
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public LocalDateTime getCreateTime() {
    return createTime;
  }

  public String getOrderNum() {
    return orderNum;
  }

  public void setOrderNum(String orderNum) {
    this.orderNum = orderNum;
  }

  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  public List<OrderEntryEntity> getEntries() {
    return entries;
  }

  public void setEntries(List<OrderEntryEntity> entries) {
    this.entries = entries;
  }

  public AddressEntity getShippingAddress() {
    return shippingAddress;
  }

  public void setShippingAddress(AddressEntity shippingAddress) {
    this.shippingAddress = shippingAddress;
  }

  public AddressEntity getBillingAddress() {
    return billingAddress;
  }

  public void setBillingAddress(AddressEntity billingAddress) {
    this.billingAddress = billingAddress;
  }

  public List<ChargeEntity> getCharges() {
    return charges;
  }

  public void setCharges(List<ChargeEntity> charges) {
    this.charges = charges;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public String getCartId() {
    return cartId;
  }

  public void setCartId(String cartId) {
    this.cartId = cartId;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public PaymentStatus getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatus paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public ShipmentStatus getShipmentStatus() {
    return shipmentStatus;
  }

  public void setShipmentStatus(ShipmentStatus shipmentStatus) {
    this.shipmentStatus = shipmentStatus;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
