package shop.velox.order.service;

public final class OrderServiceConstants {

  public static class Authorities {

    public static final String ORDER_ADMIN = "Admin_Order";
  }

  private OrderServiceConstants() {
    // private constructor to prevent instantiation
  }
}
