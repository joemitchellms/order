package shop.velox.order.api.controller.impl;

import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.commons.converter.Converter;
import shop.velox.order.api.controller.OrderController;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.model.OrderEntity;
import shop.velox.order.service.OrderService;

@RestController
public class OrderControllerImpl implements OrderController {

  private static final Logger LOG = LoggerFactory.getLogger(OrderControllerImpl.class);

  private final OrderService orderService;
  private final Converter<OrderEntity, OrderDto> orderConverter;

  public OrderControllerImpl(@Autowired OrderService orderService,
      @Autowired Converter<OrderEntity, OrderDto> orderConverter) {
    this.orderService = orderService;
    this.orderConverter = orderConverter;
  }


  @Override
  public ResponseEntity<OrderDto> createOrder(final String userId, final OrderDto orderDto) {
    OrderEntity orderEntity = orderConverter.convertDtoToEntity(orderDto);
    OrderDto createdOrderDto = orderConverter.convertEntityToDto(
        orderService.createOrder(userId, orderEntity));
    LOG.info("created order dto {}:", createdOrderDto);
    String newEtag = String.valueOf(createdOrderDto.hashCode());

    return ResponseEntity
        .status(HttpStatus.CREATED)
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.ETAG)
        .eTag(newEtag)
        .body(createdOrderDto);
  }

  @Override
  public ResponseEntity<OrderDto> getOrder(final String userId, final String orderId,
      final String cachedEtag) {
    LOG.info("getOrder {}, {}, {}", userId, orderId, cachedEtag);

    Optional<OrderEntity> orderEntity = orderService.getOrder(orderId);
    if (orderEntity.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    OrderDto orderDto = orderConverter.convertEntityToDto(orderEntity.get());
    String newEtag = StringUtils.wrap(String.valueOf(orderDto.hashCode()), '"');

    if (StringUtils.isNotBlank(cachedEtag) && cachedEtag.equals(newEtag)) {
      return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.ETAG)
        .eTag(newEtag)
        .body(orderDto);
  }


  @Override
  public Page<OrderDto> getOrders(final String userId, Pageable pageable,
      final OrderStatus filter) {
    if (StringUtils.isBlank(userId)) {
      LOG.info("get all Orders as admin {}", pageable);
      return orderConverter.convert(pageable, orderService.getAll(pageable, filter));
    }
    LOG.info("get user Orders as user {}, {}", userId, pageable);
    return orderConverter.convert(pageable,
        orderService.getAllByUserId(userId, pageable, filter));

  }

  @Override
  public ResponseEntity<OrderDto> update(String userId, String orderId, OrderDto order,
      String cachedEtag) {
    LOG.info("update {}, {}, {}", orderId, order, cachedEtag);
    if (StringUtils.isBlank(cachedEtag)) {
      return ResponseEntity.status(HttpStatus.PRECONDITION_REQUIRED).build();
    }

    OrderDto orderBeforeModification = orderConverter.convertEntityToDto(
        orderService.getOrder(orderId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    String orderBeforeModificationEtag = StringUtils.wrap(
        String.valueOf(orderBeforeModification.hashCode()), '"');
    if (!StringUtils.equals(cachedEtag, orderBeforeModificationEtag)) {
      return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(null);
    }

    OrderDto updatedOrderDto = orderConverter.convertEntityToDto(
        orderService.updateOrder(orderId, orderConverter.convertDtoToEntity(order), userId));
    String newEtag = StringUtils.wrap(String.valueOf(updatedOrderDto.hashCode()), '"');
    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.ETAG)
        .eTag(newEtag)
        .body(updatedOrderDto);
  }

}
