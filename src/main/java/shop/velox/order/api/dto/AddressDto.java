package shop.velox.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AddressDto {

  @Schema(
      description = "Unique identifier of the Address.",
      example = "a6800895-9a37-4e48-a65a-621eea12d2d4")
  private String id;

  @Schema(description = "First name of the recipient (user)", required = true)
  private String firstName;

  @Schema(description = "Last name of the recipient (user)", required = true)
  private String lastName;

  @Schema(description = "Company where the recipient (user) works", required = true)
  private String company;

  @Schema(description = "Street where the orders will be shipped", required = true)
  private String address;

  @Schema(description = "City where the orders will be shipped", required = true)
  private String city;

  @Schema(description = "Zip code of the address where the orders will be shipped", required = true)
  private String zipCode;

  @Schema(
      description = "Post Office Box of the company where the orders will be shipped",
      required = false)
  private String poBox;

  @Schema(description = "Country where the orders will be shipped", required = true)
  private String country;

  public AddressDto(){ super(); }

  public AddressDto(
      String id,
      String firstName,
      String lastName,
      String company,
      String address,
      String city,
      String zipCode,
      String poBox,
      String country) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.company = company;
    this.address = address;
    this.city = city;
    this.zipCode = zipCode;
    this.poBox = poBox;
    this.country = country;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getPoBox() {
    return poBox;
  }

  public void setPoBox(String poBox) {
    this.poBox = poBox;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
