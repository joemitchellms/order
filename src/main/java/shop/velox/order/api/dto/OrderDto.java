package shop.velox.order.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.user.api.dto.UserDto;

import static java.time.LocalDateTime.now;
import static org.apache.commons.lang3.StringUtils.substringAfterLast;


public class OrderDto {

  @Schema(description = "Unique identifier of the Order.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  @JsonInclude(Include.ALWAYS)
  private String id;

  @Schema(description = "Unique identifier of the user.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  @JsonInclude(Include.ALWAYS)
  private String userId;

  @Schema(description = "Last portion of the Unique Identifier (id)", example = "49ee128a40c9")
  @JsonInclude(Include.ALWAYS)
  private String orderNum;

  @Schema(description = "When the Order was created")
  @JsonInclude(Include.ALWAYS)
  private LocalDateTime createTime;

  @Schema(description = "List of items that user wants to buy")
  @JsonInclude(Include.ALWAYS)
  private List<OrderEntryDto> entries;

  @Schema(description = "Shipping Address")
  @JsonInclude(Include.ALWAYS)
  private AddressDto shippingAddress;

  @Schema(description = "Shipping Address")
  @JsonInclude(Include.ALWAYS)
  private AddressDto billingAddress;

  @Schema(description = "charges")
  @JsonInclude(Include.ALWAYS)
  private List<ChargeDto> charges;

  @Schema(description = "Total price that user will pay")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal totalPrice;

  @Schema(description = "Currency of the order")
  @JsonInclude(Include.ALWAYS)
  private String currencyId;

  @Schema(description = "Id of the Cart")
  @JsonInclude(Include.ALWAYS)
  private String cartId;

  @Schema(description = "Order status")
  @JsonInclude(Include.ALWAYS)
  private OrderStatus orderStatus;

  @Schema(description = "Payment status")
  @JsonInclude(Include.ALWAYS)
  private PaymentStatus paymentStatus;

  @Schema(description = "Shipment status")
  @JsonInclude(Include.ALWAYS)
  private ShipmentStatus shipmentStatus;

  @Schema(description = "User information")
  @JsonInclude(Include.ALWAYS)
  private UserDto user;

  public OrderDto() {
    super();
    this.id = UUID.randomUUID().toString();
    this.createTime = now().withNano(0);
    this.orderNum = substringAfterLast(id, "-");
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getOrderNum() {
    return orderNum;
  }

  public void setOrderNum(String orderNum) {
    this.orderNum = orderNum;
  }

  public LocalDateTime getCreateTime() {
    return createTime;
  }

  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  public List<OrderEntryDto> getEntries() {
    return entries;
  }

  public void setEntries(List<OrderEntryDto> entries) {
    this.entries = entries;
  }

  public AddressDto getShippingAddress() {
    return shippingAddress;
  }

  public void setShippingAddress(AddressDto shippingAddress) {
    this.shippingAddress = shippingAddress;
  }

  public AddressDto getBillingAddress() {
    return billingAddress;
  }

  public void setBillingAddress(AddressDto billingAddress) {
    this.billingAddress = billingAddress;
  }

  public List<ChargeDto> getCharges() {
    return charges;
  }

  public void setCharges(List<ChargeDto> charges) {
    this.charges = charges;
  }

  public String getCartId() {
    return cartId;
  }

  public void setCartId(String cartId) {
    this.cartId = cartId;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public PaymentStatus getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatus paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public ShipmentStatus getShipmentStatus() {
    return shipmentStatus;
  }

  public void setShipmentStatus(ShipmentStatus shipmentStatus) {
    this.shipmentStatus = shipmentStatus;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
