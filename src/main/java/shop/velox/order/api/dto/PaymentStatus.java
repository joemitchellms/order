package shop.velox.order.api.dto;

public enum PaymentStatus {
  NOT_PAID,
  PAID,
  REFUNDED
}
